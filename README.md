# Proofmode Cloud

## Uploading and pinning via HTTPS

You can now upload a .zip file containing the a full directory structure you want copied to IPFS. 

1. Send a PUT request to: https://d2dv6mdglezkzy.cloudfront.net/add (this is a temporary URL). The body of the request should be the .zip file data.
2. The endpoint will return a 307 redirect to a signed S3 URL which your code should follow. 
3. Once the file is uploaded to S3 it is queued to be uploaded & pinned in our cluster. Since this happens asynchronously, the endpoint doesn’t return the CID: ideally that has already been calculated elsewhere. 

## Pinning a file directly

This describes how to pin from the desktop, but the commands when using a library are the same.

1. Download and install IPFS Desktop: https://docs.ipfs.io/install/ipfs-desktop/
2. Open a Terminal and make sure that the 'ipfs' command also got installed. If not, download and install the binary here: https://docs.ipfs.io/install/command-line/
3. Add the pinning service from the command line: 

    ```
    ipfs pin remote service add gptest https://gpipfs.redaranj.com ""
    ```

    (You need those empty quotes at the end. Also, the domain is temporary but the the service is running on GP infrastructure).

4. If you look at the “Files” list in IPFS Desktop, you should see a “Set pinning” option for each file and “gptest” should be one of the pinning options. Pin a file and let me know how it goes.
5. You can also pin from the command line:

```
ipfs pin remote add your-cid-goes-here --service gptest
```


### Open questions

- Do we need to return a CID for HTTP upload?
- What metadata do we want to store with FDroid (or Proofmode) pins?
- API token management
- What domain(s) should we use for our cluster(s)?

